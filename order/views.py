from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from order.models import AcceptOrder, Order
from django.core import serializers

import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter

# Create the logger and set it's logging level
logger = logging.getLogger("logstash")
logger.setLevel(logging.DEBUG)        

# Create the handler
handler = AsynchronousLogstashHandler(
    host='8ef166a3-7883-4013-a052-9bfaf2396397-ls.logit.io', 
    port=18893,
    ssl_enable=False,
    ssl_verify=False,
    database_path='')
# Here you can specify additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# Assign handler to the logger
logger.addHandler(handler)


import json

# Create your views here.
def get_acc_order(request) :
    response = request.GET.get("id")
    if response == None :
        object_json = serializers.serialize('json',AcceptOrder.objects.all())
        return HttpResponse(object_json, content_type="text/json-comment-filtered")    
    else :
        order = AcceptOrder.objects.filter(order_id=response).filter(status=True)
        if len(order) == 1 :
            return JsonResponse(order[0].to_json())
        else :
            return HttpResponse("order hasn't accepted yet")

@csrf_exempt
def cancel_acc_order(request):
    order_id = request.GET.get("id")
    data = json.loads(request.body)
    check_order = AcceptOrder.objects.filter(order_id=order_id).filter(doctor_id = data["doctor_id"]).filter(status=True)
    if len(check_order) == 1 :
        obj = AcceptOrder.objects.get(id=check_order[0].id)
        obj.status = False
        obj.save()
        order = Order.objects.get(id=order_id)
        order.status= "Pending"
        order.save()
        try :
            logging.info("success cancel")
            return HttpResponse(200)
        except json.decoder.JSONDecodeError :
            logging.error("Error while decoding json")
            return HttpResponse("error while decoding json")
    else :
        return HttpResponse("you haven't accepted this order")    

@csrf_exempt 
def post_acc_order(request) :
    order_id = request.GET.get("id")
    data = json.loads(request.body)
    check_order = AcceptOrder.objects.filter(order_id=order_id, status = True)
    if len(check_order) == 0 :
        accept = AcceptOrder.objects.create(
            doctor_id=data["doctor_id"],
            order_id=order_id,
            status=True)
        try :
            obj = Order.objects.get(id=order_id)
            obj.status = "Diterima"
            obj.doctor_id = data["doctor_id"]
            obj.save()
            return HttpResponse(200)
        except Exception :
            logging.error("Error when saving order object")
            return HttpResponse("failed to save order object")
        
    else :
        return HttpResponse("order has already accepted")

@csrf_exempt
def post_create_order(request) :
    data = json.loads(request.body)
    Order.objects.create(
        patient_id = data['patient_id'],
        keluhan = data['keluhan'],
        riwayat = data['riwayat'],
        date = data['date'],
        no_antrian = data['no_antrian'],
        status= "Pending"
    )
    return HttpResponse(200)

def get_order(request):
    order_id = request.GET.get("id")
    if order_id == None :
        all_item = Order.objects.all()
        return HttpResponse(serializers.serialize('json', all_item), content_type="text/json-comment-filtered")
    else :
        try :
            order_id = request.GET.get("id")
            return JsonResponse(Order.objects.get(id=order_id).to_json())
        except Exception :
            logging.error("Order not exist")
            return HttpResponse(404)

