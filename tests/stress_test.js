import http from 'k6/http';
import {sleep} from 'k6';

export let options = {
    insecureSkipTLSVerify: true,
    noConnectionReuse: false,
    stages :[
        {duration: "2m", target:100},
        {duration: "2m", target:100},
        {duration: "2m", target:2000},
        {duration: "2m", target:2500},
    ],
};

export default() => {
    http.get('https://law-acc-order.herokuapp.com/order/')
    http.get('https://law-acc-order.herokuapp.com/order/?id=6')
    http.post('https://law-acc-order.herokuapp.com/order/accept?id=7',JSON.stringify({
        doctor_id: '123',
      }))
    http.post('https://law-acc-order.herokuapp.com/order/cancel?id=7',JSON.stringify({
        doctor_id: '123',
      }))
}